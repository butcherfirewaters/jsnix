/*
*
* Задание №1. Дочерние элементы в DOM
Для страницы:
<html>
<body>
 <div>Пользователи:</div>
 <ul>
 <li>Джон</li>
 <li>Пит</li>
 </ul>
</body>
</html>
Как получить:
• Напишите код, который получит элемент <div>?
• Напишите код, который получит <ul>?
• Напишите код, который получит второй <li> (с именем Пит)?
*
* */

let div = document.body.getElementsByTagName('div');
let ul = document.body.getElementsByTagName('ul');
let li = ul[0];
console.log(div[0]);
console.log(ul[0]);
console.log(li.lastElementChild);

/*
* Задание №2. Выделите ячейки по диагонали
Напишите код, который выделит красным цветом все ячейки в таблице по диагонали.
Вам нужно получить из таблицы <table> все диагональные <td> и выделить их,
используя код:
// в переменной td находится DOM-элемент для тега <td>
td.style.backgroundColor = 'red';
Результат
*
* */

let table = document.body.getElementsByClassName('table');
console.log(table[0].rows);
console.log(table[0].rows.length);
for (let i = 0; i < table[0].rows.length; i++) {
    let row = table[0].rows[i];
    row.cells[i].style.backgroundColor = 'red';
}

/*
* Задание №3. Поиск элементов
Вот документ с таблицей и формой. Как найти?…
• Таблицу с id="age-table".
• Все элементы label внутри этой таблицы (их три).
• Первый td в этой таблице (со словом «Age»).
• Форму form с именем name="search".
• Первый input в этой форме.
• Последний input в этой форме.
Используйте код файла table.html и браузерные инструменты разработчика:
<html>
<body>
 <form name="search">
 <label>Search the site:
 <input type="text" name="search">
 </label>
 <input type="submit" value="Search!">
 </form>
 <hr>
 <form name="search-person">
 Search the visitors:
 <table id="age-table">
 <tr>
 <td>Age:</td>
 <td id="age-list">
 <label>
 <input type="radio" name="age" value="young">less than
18</label>
 <label>
 <input type="radio" name="age" value="mature">18-50</label>
 <label>
 <input type="radio" name="age" value="senior">more than
50</label>
 </td>
 </tr>
 <tr>
 <td>Additionally:</td>
 <td>
 <input type="text" name="info[0]">
 <input type="text" name="info[1]">
 <input type="text" name="info[2]">
 </td>
 </tr>
 </table>
 <input type="submit" value="Search!">
 </form>
</body>
</html>
*
* */

let tableId = document.getElementById('age-table');
console.log(tableId);
console.log(tableId.getElementsByTagName('label'));
console.log(tableId.firstChild);

let form = document.getElementsByName('search')[0];
console.log(form);
console.log(form.getElementsByTagName('input')[0]);

let inputs = form.querySelectorAll('input');
let inputs2 = inputs[inputs.length-1];
console.log(inputs2);


/*
* Задание №4. Очистите элемент
Создайте функцию clear(elem), которая удаляет всё содержимое из elem.
<ol id="elem">
 <li>Привет</li>
 <li>Мир</li>
</ol>
*
* */
elem = document.getElementById('elem');
function clear(elem) {
    elem.innerHTML = '';
}

clear(elem);

console.log(elem);

/*
* Задание №5. Создайте список
Напишите интерфейс для создания списка.
Для каждого пункта:
1. Запрашивайте содержимое пункта у пользователя с помощью prompt.
2. Создавайте элемент <li> и добавляйте его к <ul>.
3. Процесс прерывается, когда пользователь нажимает Esc или вводит пустую
строку.
Все элементы должны создаваться динамически.
Если пользователь вводит HTML-теги -– пусть в списке они показываются как обычный
текст
*
* */

let newElementUl = document.createElement('ul');
document.body.append(newElementUl);

while (true) {
    let data = prompt("Введите текст для элемента списка", "");

    if (!data) {
        break;
    }

    let newElementLi = document.createElement('li');
    newElementLi.textContent = data;
    newElementUl.append(li);
}

/*
*   Задание №6. Вставьте HTML в список
Напишите код для вставки <li>2</li><li>3</li> между этими двумя <li>:
<ul id="ul">
 <li id="one">1</li>
 <li id="two">4</li>
</ul>
*
* */

let ulNew = document.getElementById('one');
ulNew.insertAdjacentHTML('afterend', '<li>2</li><li>3</li>');

/*
* Задание №7. Создать уведомление
Напишите функцию showNotification(options), которая создаёт уведомление: <div
class="notification"> с заданным содержимым. Уведомление должно автоматически
исчезнуть через 1,5 секунды
*
* */


/*Пример объекта options:
// показывает элемент с текстом "Hello" рядом с правой верхней частью окна.
showNotification({
 top: 10, // 10px от верхней границы окна (по умолчанию 0px)
 right: 10, // 10px от правого края окна (по умолчанию 0px)
 html: "Hello!", // HTML-уведомление
 className: "welcome" // дополнительный класс для div (необязательно)
})
*
*
* */


/*не смог сделать*/