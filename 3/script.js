/*
* Задание №1. Проверка на пустоту
Напишите функцию isEmpty(obj), которая возвращает true, если у объекта нет свойств,
иначе false.
* */

let obj = {
    'vasia': 23,
    'age': 25,
};
function isEmpty(obj) {
    for (let svoistvo in obj) {
        return false;
    }
    return true;
}
alert( isEmpty(obj) ); // true

/*
* Задание №2. Умножаем все числовые свойства на 2
Создайте функцию multiplyNumeric(obj), которая умножает все числовые свойства
объекта obj на 2.
Обратите внимание, что multiplyNumeric не нужно ничего возвращать. Следует
напрямую изменять объект.
P.S. Используйте typeof для проверки, что значение свойства числово
*
* */

function multiplyNumeric(obj) {
    for (let svoistvo in obj) {
        if (typeof obj[svoistvo] === 'number'){
            obj[svoistvo] *= 2;
        }
    }
}

multiplyNumeric(obj);
for (let key in obj){
    alert(obj[key]);
}

/*
* Задание №3. Ввод числового значения
Создайте функцию readNumber, которая будет запрашивать ввод числового значения
до тех пор, пока посетитель его не введёт.
функция должна возвращать числовое значение.
Также надо разрешить пользователю остановить процесс ввода, отправив пустую
строку или нажав «Отмена». В этом случае функция должна вернуть null.
*
* */

function readNumber() {
    let znach = prompt('Введите числовое значение');
    znach = Number(znach);
    alert(typeof znach);
    let newTypeOfZnach = typeof znach;
    if (typeof znach == 'number' || znach === ''){
        if (isNaN(znach)) {
            readNumber();
        }
        alert(znach);
        return znach;
    } else if (isNaN(newTypeOfZnach)) {
        readNumber();
    }
}

readNumber();

/*
* Задание №4. Случайное число от min до max
Встроенный метод Math.random() возвращает случайное число от 0 (включительно) до
1 (но не включая 1)
Напишите функцию random(min, max), которая генерирует случайное число с
плавающей точкой от min до max (но не включая max).
Пример работы функции:
alert( random(1, 5) ); // 1.2345623452
alert( random(1, 5) ); // 3.7894332423
alert( random(1, 5) ); // 4.3435234525
*
* */

function randomMethod(min, max) {
    return min + Math.random() * (max - min);
}

alert(randomMethod(1,5));

/*
* Задание №5. Случайное целое число от min до max
Напишите функцию randomInteger(min, max), которая генерирует случайное целое
(integer) число от min до max (включительно).
Любое число из интервала min..max должно появляться с одинаковой вероятностью.
Пример работы функции:
alert( randomInteger(1, 5) ); // 1
alert( randomInteger(1, 5) ); // 3
alert( randomInteger(1, 5) ); // 5
*
* */

function randomMethodIndexer(min, max) {
    let chislo = min + Math.random() * (max - min);
    return Math.floor(chislo);
}

alert(randomMethodIndexer(1,5));

/*
*
* адание №6. Сделать первый символ заглавным
Напишите функцию ucFirst(str), возвращающую строку str с заглавным первым
символом.
Например:
ucFirst("вася") == "Вася"
* */

function strokaToUpfirst(stroka) {
    return stroka.charAt(0).toUpperCase() + stroka.slice(1);
}

alert(strokaToUpfirst('kolya'));

/*
* Задание №7. Проверка на спам
Напишите функцию checkSpam(str), возвращающую true, если str содержит 'viagra' или
'XXX', а иначе false.
Функция должна быть нечувствительна к регистру:
checkSpam('buy ViAgRA now') == true
checkSpam('free xxxxx') == true
checkSpam("innocent rabbit") == false
*
* */

function checkSpam(str) {
    let NewStr = str.toLowerCase();
    let viagra = 'ViAgRA'.toLowerCase();
    let xxx = 'XXX'.toLowerCase();
    if (NewStr.includes(viagra) || NewStr.includes(xxx)){
        return true;
    } else {
        return false
    }
}

alert(checkSpam('ssdad'));

/*
* Задание №8. Усечение строки
Создайте функцию truncate(str, maxlength), которая проверяет длину строки str и, если
она превосходит maxlength, заменяет конец str на "…", так, чтобы её длина стала равна
maxlength.
Результатом функции должна быть та же строка, если усечение не требуется, либо,
если необходимо, усечённая строка.
Например:
truncate("Вот, что мне хотелось бы сказать на эту тему:", 20) = "Вот, что мне
хотело…"
truncate("Всем привет!", 20) = "Всем привет!"
* */

function truncate(str, maxlength) {
    let NewStr = str.length;
    if (NewStr > maxlength){
        return str.slice(0, maxlength);
    }
}

alert(truncate("Вот, что мне хотелось бы сказать на эту тему:", 20));

/*
* Задание №9. Выделить число
Есть стоимость в виде строки "$120". То есть сначала идёт знак валюты, а затем –
число.
Создайте функцию extractCurrencyValue(str), которая будет из такой строки выделять
числовое значение и возвращать его.
Например:
alert( extractCurrencyValue('$120') === 120 ); // true
* */

function extractCurrencyValue(str) {
    let newStr = Number(str.slice(1));
    console.log(newStr);
    console.log(typeof newStr);
    if (typeof newStr === 'number'){
        return newStr;
    } else {
        return false;
    }
}

alert(extractCurrencyValue('$120')); // true

/*
* Задание №10. Сумма введённых чисел
Напишите функцию sumInput(), которая:
• Просит пользователя ввести значения, используя prompt и сохраняет их в
массив.
• Заканчивает запрашивать значения, когда пользователь введёт не числовое
значение, пустую строку или нажмёт «Отмена».
• Подсчитывает и возвращает сумму элементов массива.
• P.S. Ноль 0 – считается числом, не останавливайте ввод значений при вводе «0»
* */

function sumInput() {

    let numbers = [];

    while (true) {

        let value = prompt("Введите число");

        // Прекращаем ввод?
        if (value === "" || value === null || !isFinite(value)) {
            break;
        }

        numbers.push(value);
    }

    let sum = 0;
    for (let number of numbers) {
        sum += Number(number);
    }
    return sum;
}

alert( sumInput() );

/*
* Задание №11. Подмассив наибольшей суммы
На входе массив чисел, например: arr = [1, -2, 3, 4, -9, 6].
Задача: найти непрерывный подмассив в arr, сумма элементов в котором максимальна.
Функция getMaxSubSum(arr) должна возвращать эту сумму.
Например:
getMaxSubSum([-1, 2, 3, -9]) = 5 (сумма выделенных)
getMaxSubSum([2, -1, 2, 3, -9]) = 6
getMaxSubSum([-1, 2, 3, -9, 11]) = 11
getMaxSubSum([-2, -1, 1, 2]) = 3
getMaxSubSum([100, -9, 2, -3, 5]) = 100
getMaxSubSum([1, 2, 3]) = 6 (берём все)
Если все элементы отрицательные – ничего не берём(подмассив пустой) и сумма равна
«0»:
getMaxSubSum([-1, -2, -3]) = 0
Попробуйте придумать быстрое решение: O(n2), а лучше за О(n) операци
*
* */

function getMaxSubSum(arr) {
    let maxSum = 0;

    for (let i = 0; i < arr.length; i++) {
        if ((arr[i] > 0 && arr[i+1] > 0) || (arr[i] > 0 && arr[i-1] > 0)){
            maxSum += arr[i];
        }
    }

    return maxSum;
}

alert( getMaxSubSum([-1, 2, 3, -9]) ); // 5

/*
* Задание №12. Обязателен ли "else"?
Следующая функция возвращает true, если параметр age больше 18.
В ином случае она запрашивает подтверждение через confirm и возвращает его
результат:
function checkAge(age) {
 if (age > 18) {
 return true;
 } else {
 // ...
 return confirm('Родители разрешили?');
 }
}
Будет ли эта функция работать как-то иначе, если убрать else?
function checkAge(age) {
 if (age > 18) {
 return true;
 }
 // ...
 return confirm('Родители разрешили?');
}
Есть ли хоть одно отличие в поведении этого варианта?
* */

function checkAge(age) {
    if (age > 18) {
        return true;
    } else {
        // ...
        return confirm('Родители разрешили?');
    }
}

checkAge(19);

function checkAge2(age) {
    if (age > 18) {
        return true;
    }
    // ...
    return confirm('Родители разрешили?');
}

checkAge2(19);

/* Ответ - функция работает также - разницы нет*/


/*
* Задание №14. Функция min(a, b)
Напишите функцию min(a,b), которая возвращает меньшее из чисел a и b.
Пример вызовов:
min(2, 5) == 2
min(3, -1) == -1
min(1, 1) == 1
* */

function minF(a, b) {
    if (a < b){
        return a;
    } else {
        return b
    }
}

alert(minF(35, 36));

/*
* Задание №15. Функция pow(x,n)
Напишите функцию pow(x,n), которая возвращает x в степени n. Иначе говоря,
умножает x на себя n раз и возвращает результат.
pow(3, 2) = 3 * 3 = 9
pow(3, 3) = 3 * 3 * 3 = 27
pow(1, 100) = 1 * 1 * ...* 1 = 1
Создайте страницу, которая запрашивает x и n, а затем выводит результат pow(x,n).
Запустить демо
P.S. В этой задаче функция обязана поддерживать только натуральные значения n, т.е.
целые от 1 и выше
* */

function pow(x, n) {
    return Math.pow(x,n);
}

alert(pow(2,3));

/*
* Задание №16. Перепишите с использованием функции-стрелки
Замените код Function Expression стрелочной функцией:
function ask(question, yes, no) {
 if (confirm(question)) yes()
 else no();
}
ask(
 "Вы согласны?",
 function() { alert("Вы согласились."); },
 function() { alert("Вы отменили выполнение."); }
);

*
* */

function ask(question, yes, no) {
    if (confirm(question)) yes();
    else no();
}

ask(
    "Вы согласны?",
    () => alert("Вы согласились."),
    () => alert("Вы отменили выполнение.")
);

/*
* Задание №17. Робинзон Крузо
Ваш месячный доход - 3333 попугая. Вы хотите купить пальму за 8000
попугаев. Вычислите, за какой промежуток времени вы насобираете на
пальму, при условии что ваши ежемесячные расходы составляют 1750
попугаев.
*
* */

function howMany() {
    let money = 0;
    for (let money = 0, i = 0; i < 8000; i++) {
        money += 3333 - 1750;
        if (money > 8000){
            return 'вы насобирали денег за '+ i + ' месяцев';
        } else {
            continue;
        }
    }
}

alert(howMany());

/*
* Задание №18. Вопросы пользователю
1. Спросить у пользователя 10 чисел
2. Если число
- положительное —> ничего не делать
- отрицательная —> получить их сумму
3. Вывести сумму отрицательных чисел, которые ввел пользователь
ВАЖНО: по условиям задачи в вашем коде должен быть только 1 prompt и
только 1 цикл for
*
* */

function f1() {
    let summer = 0;
    for (let i = 0; i <= 10; i++){
        let chislo = prompt('Введите число');
        if (chislo > 0){
            summer += Number(chislo);
        } else {
            return summer;
        }
    }
    return summer
}
alert(f1());

/*
* Задание №19. Скопирован ли массив?
Что выведет следующий код?
let fruits = ["Яблоки", "Груша", "Апельсин"];
// добавляем новое значение в "копию"
let shoppingCart = fruits;
shoppingCart.push("Банан");
// что в fruits?
alert( fruits.length ); // ?
*
* */

let fruits = ["Яблоки", "Груша", "Апельсин"];

let shoppingCart = fruits;
shoppingCart.push("Банан");
// что в fruits?
alert( fruits.length );
console.log(fruits);

/*ответы - там уже бананы, т.к. это ссылка*/

/*
* Задание №20. Операции с массивами
Давайте произведём 5 операций с массивом.
Создайте массив styles с элементами «Джаз» и «Блюз».
Добавьте «Рок-н-ролл» в конец.
Замените значение в середине на «Классика». Ваш код для поиска значения в
середине должен работать для массивов с любой длиной.
Удалите первый элемент массива и покажите его.
Вставьте «Рэп» и «Регги» в начало массива.
Массив по ходу выполнения операций:
Джаз, Блюз
Джаз, Блюз, Рок-н-ролл
Джаз, Классика, Рок-н-ролл
Классика, Рок-н-ролл
Рэп, Регги, Классика, Рок-н-ролл
*
* */

let mass = ["Джаз", "Блюз"];
mass.push("Рок-н-ролл");
mass[Math.floor((mass.length - 1) / 2)] = "Классика";
alert( mass.shift() );
mass.unshift("Рэп", "Регги");
console.log(mass);

/*
*
* Задание №21. Вызов в контексте массива
Каков результат? Почему?
let arr = ["a", "b"];
arr.push(function() {
 alert( this );
})
arr[2](); //
*
* */

let arr = ["a", "b"];
arr.push(function() {
    alert( this );
});
arr[2](); //

/*во второй ячейке находится функция вместе с нашим массивом, которую мы положили*/